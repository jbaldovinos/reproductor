from sqlite3.dbapi2 import Time
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.expected_conditions import presence_of_element_located
from selenium.webdriver.support.wait import WebDriverWait


class BandcampResultPage:

    cancion_Xpath = "//li[2]//div[1]//div[2]"
    boton_reproducir_xpath = "//div[@class='playbutton']"

    def __init__(self, driver):
        self.driver = driver


    # This method play the song
    def reproductor(self, reproducir):
        try:
            reproducir = input("Quieres reproducir canción: si/no")
            wait = WebDriverWait(self.driver, 40)
            cancion_XpathElement = wait.until(presence_of_element_located((By.XPATH, self.cancion_Xpath)))
            if reproducir == "si":
                self.cancion_XpathElement.click()
                reproduccion_xpathElement = wait.until(presence_of_element_located(By.XPATH, self.boton_reproducir_xpath))
                self.reproduccion_xpathElement.click()
                Time.sleep(100)

            else:
                print("Gracias")
                pass
        except:
            print("Incorrect List")
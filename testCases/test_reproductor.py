import unittest
import time
from selenium import webdriver
from pageObjects.HomePage import ReproductorHomePage
from pageObjects.ResultPage import BandcampResultPage


class Test_Reproductor(unittest.TestCase):
    mombreArtista = input("Escribe el nombre de un artista  ")
    baseURL = "https://bandcamp.com/"
    driver = webdriver.Chrome('C:\\Users\\jbaldovinos\\Desktop\\drivers\\chromedriver.exe')

    @classmethod
    def setUpClass(cls) -> None:
        cls.driver.get(cls.baseURL)
        cls.driver.maximize_window()

    # Test the search with wordSearch value
    def test_search(self):
        reproductorHomePage = ReproductorHomePage(self.driver)
        reproductorHomePage.sendInput(self.mombreArtista)
        reproductorHomePage.clickSearch()
        self.assertEqual("Search: " + self.mombreArtista + " | Bandcamp", self.driver.title,
                         "You are not in the search results")
        time.sleep(5)

    # Test the result list
    #def test_list(self):
        #reproductorHomePage = ReproductorHomePage(self.driver)
        #pageResults = reproductorHomePage.resultList(self.mombreArtista)
        #print("pageResults")

    # Test select one option to play
    time.sleep(5)
    def test_reproductor(self):
        time.sleep(4)
        reproducirMusica = ReproductorHomePage(self.driver)
        reproducirMusica.reproductor(self.driver)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.driver.quit()
        print("The browser is closed")
